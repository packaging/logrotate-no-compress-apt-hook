#!/bin/bash

_prepare() {
  apk --no-cache add build-base libffi-dev ruby-dev tar git curl
  gem install fpm --no-ri --no-rdoc
}

_fpm() {
  fpm -s dir -t deb --name "logrotate-no-compress-apt-hook" --version "$CI_BUILD_TAG" --vendor "morph027" --maintainer "morph027 <morphsen@gmx.com>" --description "APT Hook to disable logrotate compression" --url "https://forum.proxmox.com/threads/hint-zfs-w-lz4-vs-logrotate-gzip.30895/" --prefix "/" -C "${CI_PROJECT_DIR}/package_root" etc usr
}

which fpm || _prepare

_fpm
