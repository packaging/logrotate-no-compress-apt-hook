#!/bin/bash

# disable logfile compression for better deduplicatability
for file in /etc/logrotate.d/*
do
    sed -i -E 's/(\s+)(delay|)compress/\1#\2compress/' $file
    grep shred >/dev/null $file || sed -i 's,},\tpreremove\n\t\ttest -f "$1" \&\& shred --iteration=1 --zero "$1"\n\tendscript\n},' $file
done
