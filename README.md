[ZFS with lz4 and logrotate](https://github.com/zfsonlinux/zfs/wiki/Ubuntu-16.04-Root-on-ZFS#step-8-full-software-installation) says:

> As /var/log is already compressed by ZFS, logrotate’s compression is going to burn CPU and disk I/O for (in most cases) very little gain. Also, if you are making snapshots of /var/log, logrotate’s compression will actually waste space, as the uncompressed data will live on in the snapshot. You can edit the files in /etc/logrotate.d by hand to comment out compress, or use this loop (copy-and-paste highly recommended):
>
>...

Proxmox forums member LnxBil posted [this useful apt hook](https://forum.proxmox.com/threads/hint-zfs-w-lz4-vs-logrotate-gzip.30895/#post-154624), so here's how to simply build the package (can be found in [Pipelines](https://gitlab.com/packaging/logrotate-no-compress-apt-hook/pipelines) section)!